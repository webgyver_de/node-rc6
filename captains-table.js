var rc6 = require('./lib/index');

// register devices
// currently Philips hue lights or Android devices (with enabled network debugging), e.g. Amazon Fire TV
var fireTV = rc6.device('adb-shell', '192.168.178.20', __dirname + '/bin/' + require('os').platform() + '-' + require('os').arch() + '/adb');
var hue = rc6.device('hue', '192.168.178.70');

var kodi = {
  // FireTV Remote
  c:          'input keyevent KEYCODE_MENU',                 // context menu
  'return':   'input keyevent KEYCODE_DPAD_CENTER',          // ok
  backspace:  'input keyevent KEYCODE_BACK',                 // back
  up:         'input keyevent KEYCODE_DPAD_UP',              // up
  down:       'input keyevent KEYCODE_DPAD_DOWN',            // down
  left:       'input keyevent KEYCODE_DPAD_LEFT',            // left
  right:      'input keyevent KEYCODE_DPAD_RIGHT',           // right
  space:      'input keyevent KEYCODE_MEDIA_PLAY_PAUSE',     // play/pause
  r:          'input keyevent KEYCODE_MEDIA_REWIND',         // rewind
  f:          'input keyevent KEYCODE_MEDIA_FAST_FORWARD',   // fast forward

  // Additional keys
  i:          'input keyevent KEYCODE_I',                    // info
  l:          'input keyevent KEYCODE_L',                    // next subtitle
  m:          'input keyevent KEYCODE_M',                    // controls
  q:          'input keyevent KEYCODE_Q',                    // queue
  t:          'input keyevent KEYCODE_T',                    // toggle subtitles
  w:          'input keyevent KEYCODE_W',                    // mark watched
  x:          'input keyevent KEYCODE_X',                    // stop
  escape:     'input keyevent KEYCODE_ESCAPE',               // prev menu

  // Special commands
  p:          'input keyevent KEYCODE_POWER',                // power
  k:          'am start -n org.xbmc.kodi/.Splash',           // start kodi (fka xbmc) 
};

// colorful console messages
console.green = function(){console.log.apply(console,[].slice.call(arguments).map(function(arg,i){return (i===0)?'\u001b[32m'+arg+'\u001b[39m':arg}));};
console.red = function(){console.log.apply(console,[].slice.call(arguments).map(function(arg,i){return (i===0)?'\u001b[31m'+arg+'\u001b[39m':arg}));};
console.yellow = function(){console.log.apply(console,[].slice.call(arguments).map(function(arg,i){return (i===0)?'\u001b[33m'+arg+'\u001b[39m':arg}));};

// rc6 events
rc6.on('start', function(){
  console.log('node-rc6 waiting for input...');
}).on('end', function(){
  hue.stop();
  console.log('bye.');
}).on('input', function(key){

  // handle rc6 inputs
  if (key in kodi) {
    // FireTV
    console.green('`%s` %s', key, 'adb shell ' + kodi[key]);
    fireTV.execute(kodi[key]);
  } else if (key.match(/[0-9+-]/) !== null) {
    // hue
    if (key.match(/[1-8]/) !== null) {
      console.green('`%s` %s', key, 'adb scene ' + key);
      hue.scene(parseInt(key, 10));
    } else if (key === '0') {
      console.green('`%s` %s', key, 'hue off');
      hue.on(false);
    } else if (key === '9') {
      console.green('`%s` %s', key, 'hue random');
      hue.random(false, [1000,5000]);
    } else if (key === '+') {
      console.green('`%s` %s', key, 'hue bri +');
      hue.bri('+', 25);
    } else if (key === '-') {
      console.green('`%s` %s', key, 'hue bri -');
      hue.bri('-', 25);
    }
  } else {
    console.red('`%s` - no action', key);
  }

}).on('info', function(message, scope){
  console.log(message);
}).on('debug', function(message, scope){
  console.yellow('%s → %j', message, scope);
}).on('error', function(message, scope){
  console.red('%s\n→ %j', message, scope);
}).observe();
