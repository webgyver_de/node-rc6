var keypress = require('keypress')(process.stdin);
var events = require('events');
var util = require('util');


var RC6 = function(){};
util.inherits(RC6, events.EventEmitter);

function rc6events(rc6) {
  var self = this;

  function trim(s) {
    return s.replace(/^\s+|\s+$/, '');
  }

  this.error = function(err, scope) {
    if (err) {
      message = trim(err.stack||err.message||err);
      if (message !== '') {
        rc6.emit('error', message, scope||{});
      }
    }
  };
  this.debug = function(message, scope) {
    if (message) {
      message = trim(message);
      if (message !== '') {
        rc6.emit('debug', message, scope||{});
      }
    }
  };
  this.info = function(message, scope) {
    if (message) {
      message = trim(message);
      if (message !== '') {
        rc6.emit('info', message, scope||{});
      }
    }
  };
}

RC6.prototype.device = function() {
  var args = [].slice.call(arguments), device = args.shift();
  args.unshift(new rc6events(this));
  return require('./'+device).getInstance.apply(null, args);
};

RC6.prototype.observe = function() {
  var self = this;
  process.stdin.on('keypress', function (ch, key) {
    if (key && key.ctrl && key.name == 'c') {
      self.emit('end');
      process.stdin.pause();
    } else {
      self.emit('input', key ? key.name || key.sequence || ch : ch);
    }
  });
  process.stdin.setRawMode(true);
  process.stdin.resume();
  self.emit('start');
};

exports = module.exports = new RC6();
