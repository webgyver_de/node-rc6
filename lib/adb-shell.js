exports = module.exports = (function(){
  var exec = require('child_process').exec;

  function adbShell(ev, ip, adb_path) {

    function _shell(cmd, re) {
      var shell_cmd = (re ? adb_path + ' connect ' + ip + ' && ' : '') + adb_path + ' shell ' + cmd;
      ev.debug('adb shell ' + cmd, { cmd: shell_cmd });

      exec(shell_cmd, function(error, stdout, stderr) {
        ev.info(stdout, { cmd: shell_cmd });
        if (!re && (error !== null) || stderr !== '') {
          ev.debug('reconnect', { cmd: shell_cmd, error: error.message||error||'', stderr: stderr });
          ev.info('reconnect', { cmd: shell_cmd });
          _shell(cmd, true);
        } else if (error !== null) {
            ev.error(error, { cmd: shell_cmd, stderr: stderr });
        } else if (stderr !== '') {
          ev.error(stderr, { cmd: shell_cmd });
        }
      });
    }

    this.execute = function(command) {
      _shell(command);
    }
  }

  return {
    getInstance: function(ev, ip, adb_path) {
      return new adbShell(ev, ip, adb_path);
    }
  };
})();
