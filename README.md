# node-rc6

Super small home automation project, that let my Logitech Harmony One control Amazon Fire TV / Kodi Mediacenter and Philips hue lights.
Runs on a Raspberry Pi with a noname RC6 USB IR receiver.

RC6 labeled IR receiver simple convert IR commands into keystrokes and is often used in connection with remote controls for Windows Media Center / Kodi / XBMC.

This package uses thouse IR inputs to control various devices in the local network.


## Installation

```javascript
npm install node-rc6
```

You also need the Android Debug Bridge (adb) as binary[^1].


## Usage

You have to create _devices_, currently Philips hue lights and Android devices (e.g. Amazon Fire TV) are supported. Every RC6 input (even from a normal keyboard) fires an _input_ event. Use the `key` to send an associated command to a _device_.

```javascript
var rc6 = require('node-rc6');

// register devices
var fireTV = rc6.device('adb-shell', '192.168.x.x', './bin/adb');
var hue = rc6.device('hue-api', '192.168.x.x');

// event driven input observation
rc6.on('input', function(key){  
  // key corresponds the key on the keyboard
  if (key === 'enter') {
    // sends a keyevent to the Fire TV
    fireTV.execute('input keyevent KEYCODE_DPAD_CENTER');
  } else if (key === 'k') {
    // try to start Kodi on Fire TV
    fireTV.execute('am start -n org.xbmc.kodi/.Splash');
  } else if (key.match(/[01]/) !== null) {
    // turn hue lights on/off
    hue.on(key === '1');
  }
}).observe();
```

## API
_node-rc6 inherits [EventEmiter](http://nodejs.org/api/events.html)_

method | arguments | description
-------|-----------|------------
`device` | _string_ `type`, _string_ `ip`, ... | create a new device (s. [Devices](#devices))
`observe` | _void_ | start the RC6 input observer


### Events

event | arguments | description
------|-----------|------------
_`start`_ | _void_ | observation starts
_`end`_ | _void_ | observation ends (`ctrl+c`)
_`input`_ | _string_ `key ` | keystorke, `key` corresponds the name of a key on normal keyboard
_`info`_ | _string_ `message`, _object_ `scope` | info messages like _child_process_ output, `scope` has information around the event, e.g. the returned _json_ from the hue API
_`error`_ | _string/Error_ `error`, _object_ `scope` | error reporting, `scope` has information around the event, e.g. the returned _json_ from the hue API
_`debug`_ | _string_ `message`, _object_ `scope` | debug messages, `scope` has information around the event, e.g. the returned _json_ from the hue API


### [Devices](id:devices)

#### adb-shell `device('adb-shell', ip, path_to_adb);`
_Used for Android devices which have network debugging enabled._

method | arguments | description
-------|-----------|------------
`execute`| _string_ `command` | executes a [_adb_ shell command](http://developer.android.com/tools/help/adb.html#shellcommands)



#### hue `device('hue', ip);`
_Philips hue lights._

method | arguments | description
-------|-----------|------------
`on` | _boolean_ `on`| turn lights on/off
`scene` | _int_ `num`| [My hue](https://my.meethue.com/en-us/my-scenes) scene (1.._n_)
`bri` | _char_ `operation`, _int_ `value` | change brightness by _value_ using `'='`, `'+'` or `'-'` _operation_
`random`| _boolean_ `sync`, _mixed_ `duration`, _int_ _`transition`_ | bring the hue bulbs (simultaneously or separately, use `sync`) to random colors, every milliseconds or random ms values (e.g. `duration => [1000,3000]`), use optionally `transition` to have always the same time for the transition effect (otherwise the transition is permanent)


## Status

This is the first implementation. The logging is still silly, only two _devices_ with micro API functionality. But it works perfect for me. No further work planed.

My personal config can be found in `captains-table.js.


## Links

- [Install Kodi on Amazon Fire TV](http://kodi.wiki/view/Amazon_Fire_TV)
- [Philips hue API](http://www.developers.meethue.com/philips-hue-api)



[^1]: For my environment I found `adb` for my Mac in the [HOW-TO:Install Kodi on Fire TV (4.1 Setup ADB on Your Host)](http://kodi.wiki/view/HOW-TO:Install_Kodi_on_Fire_TV#Setup_ADB_on_Your_Host) and for the Raspberry Pi in the [xda-developers community](http://forum.xda-developers.com/showthread.php?t=1924492).
